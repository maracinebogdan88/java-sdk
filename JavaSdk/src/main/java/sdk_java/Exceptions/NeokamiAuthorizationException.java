package sdk_java.Exceptions;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright 2015 Neokami Inc.
 */

public class NeokamiAuthorizationException extends NeokamiBaseException {

    public NeokamiAuthorizationException(JSONObject jsonResponseObj, int code) throws JSONException {
        super(jsonResponseObj, code);
    }
}
