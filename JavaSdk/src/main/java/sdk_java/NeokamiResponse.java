package sdk_java;

/**
 * Copyright 2015 Neokami GmbH.
 */

import org.json.JSONException;
import org.json.JSONObject;
import sdk_java.Exceptions.*;

import java.util.Map;


public class NeokamiResponse extends Base {

    private String output_format;
    private String jsonResponse;
    private boolean silentFails;
    private JSONObject jsonResponseObj = null;


    public NeokamiResponse(String jsonResponse, String output_format, boolean silentFails) throws JSONException, NeokamiBaseException {

        this.jsonResponse = jsonResponse;
        this.silentFails = silentFails;
        this.output_format = output_format;
        this.jsonResponseObj = this.createJsonObject(jsonResponse);
        this.validate();
    }

    public JSONObject createJsonObject(String jsonResponse) throws JSONException, NeokamiServerException {

        if (jsonResponse == null || jsonResponse.length() == 0) {

            throw new NeokamiServerException(null, 503);
        }
        return new JSONObject(jsonResponse);
    }

    public int status() throws JSONException {

        return this.jsonResponseObj.getInt("status_code");

    }

    public int retries() throws JSONException, NeokamiServerException {

        if (this.warnings() != null) {

            return this.createJsonObject(warnings()).getInt("retries");
        }
        return 0;
    }

    public String warnings() throws JSONException {

        return this.jsonResponseObj.getString("warnings");
    }

    public String errors() throws JSONException {

        if (hasError()) {
            return this.jsonResponseObj.getString("errors");
        }
        return null;
    }

    public boolean hasError() throws JSONException {

        return this.jsonResponseObj.has("errors");
    }

    public String result() throws JSONException {

        return this.jsonResponseObj.getString("result");
    }

    public String getJobIdResult() throws JSONException, NeokamiServerException {

        if (this.result() != null && this.result().contains("job_id")) {

            return this.createJsonObject(result()).getString("job_id");
        }
        return null;
    }

    private boolean validate() throws JSONException, NeokamiBaseException {

        if (this.silentFails) {

            return true;
        }

        switch (this.status()) {

            case 400:
                throw new NeokamiSDKException(this.jsonResponseObj, 400);
            case 401:
                throw new NeokamiAuthorizationException(this.jsonResponseObj, 401);
            case 402:
                throw new NeokamiBlockedException(this.jsonResponseObj, 402);
            case 403:
                throw new NeokamiBlockedException(this.jsonResponseObj, 403);
            case 426:
                throw new NeokamiSDKException(this.jsonResponseObj, 426);
            case 500:
                throw new NeokamiServerException(this.jsonResponseObj, 500);
            default:
                return true;
        }

    }

}
