import junit.framework.TestCase;
import org.json.JSONException;
import org.junit.Test;
import sdk_java.Exceptions.NeokamiAuthorizationException;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.ImageAnalyser;
import sdk_java.NeokamiResponse;

import java.io.IOException;

/**
 * Copyright 2015 Neokami GmbH.
 */

public class NeokamiExceptionsTest extends TestCase {

    @Test
    public void test_authorization_exception_silent() throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {


            NeokamiResponse neokamiResponse;
            ImageAnalyser imageAnalyser = new ImageAnalyser();

            String workingDir = System.getProperty("user.dir");
            imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
            imageAnalyser.setWait(0);
            imageAnalyser.setSilentFails(true);
            neokamiResponse = imageAnalyser.analyseFromDisk();

            assertTrue(neokamiResponse.hasError());
            assertEquals(401, neokamiResponse.status());
            assertNotNull(neokamiResponse.result());
            imageAnalyser.setApiKey("invalid");

            assertTrue(neokamiResponse.hasError());
            assertEquals(401, neokamiResponse.status());
            assertNotNull(neokamiResponse.result());
            assertTrue(neokamiResponse.errors() instanceof String);

    }

    @Test
    public void test_authorization_exception() throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {


        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();
        imageAnalyser.setSilentFails(true);
        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
        imageAnalyser.setWait(0);

        imageAnalyser.setApiKey("invalid");
        neokamiResponse=imageAnalyser.analyseFromDisk();

    }

    @Test
    public void test_authorization_exception_format() throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {

        try {
            NeokamiResponse neokamiResponse;
            ImageAnalyser imageAnalyser = new ImageAnalyser();

            String workingDir = System.getProperty("user.dir");
            imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
            imageAnalyser.setSilentFails(true);
            imageAnalyser.setWait(0);
            imageAnalyser.setApiKey("invalid");
            neokamiResponse = imageAnalyser.analyseFromDisk();
        }catch (NeokamiAuthorizationException e){

            assertEquals(401,e.getStatusCode());
            assertFalse(e.isMalformed());
            assertTrue(e.getMessage() instanceof String);
            assertEquals("[]",e.getWarnings());
            assertNotNull(e.getError());

        }


    }


}