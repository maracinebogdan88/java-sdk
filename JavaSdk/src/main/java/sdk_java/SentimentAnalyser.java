package sdk_java;

import org.json.JSONException;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.HttpClients.NeokamiCurl;

import java.io.IOException;
import java.util.HashMap;

/**
 * Copyright 2015 Neokami GmbH.
 */

public class SentimentAnalyser extends NeokamiRequest {

    protected int sentences;
    protected String text;

    public NeokamiResponse analyse() throws JSONException, NeokamiBaseException, NeokamiParametersException, IOException {


        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("wait", String.valueOf(this.getWait()));
        hm.put("max_retries", String.valueOf(this.getMaxRetries()));
        hm.put("sleep", String.valueOf(this.getSleep()));
        hm.put("api_key", this.getApikey());
        hm.put("sdk_version", Base.SDK_VERSION);
        hm.put("sdk_lang", Base.SDK_LANG);
        hm.put("text", this.getText());
        hm.put("sentences", String.valueOf(this.getSplitText()));

        NeokamiCurl neokamiCurl = new NeokamiCurl();
        String jsonResponse;
        SentimentAnalyser se = new SentimentAnalyser();
        se.checkHasAllParameters(hm);

        jsonResponse = neokamiCurl.post(this.getUrl("/analyse/text/sentiment"), hm);
        return new NeokamiResponse(jsonResponse, this.getOutputFormat(), this.getSilentFails());
    }

    public void setSplitText(int sentences) {
        this.sentences = sentences;
    }

    public int getSplitText() {
        return this.sentences;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }


}
