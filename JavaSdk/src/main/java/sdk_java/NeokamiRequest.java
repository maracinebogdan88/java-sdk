package sdk_java;

/**
 * Copyright 2015 Neokami GmbH.
 */

import org.json.JSONException;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.HttpClients.NeokamiCurl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NeokamiRequest extends Base {

    //if set to 0, analysis is queued and can be retrieved at a later time
    //using the job id; otherwise endpoint tries $max_retries number of times
    //before returning
    private int wait = 1;
    //time in seconds to wait between retries for results
    private float sleep = 1;
    //maximum number to retry for results if wait is set to 1
    private int max_retries = 5;
    //format, can be json, array or xml
    private String output_format = "array";
    //api result output type, can be xml, json
    private String output_type;
    //the api key, get yours now @ www.neokami.com
    private String apiKey;
    //if set to true, exceptions will be suppressed
    private boolean silentFails = false;


    public void setApiKey(String apiKey) {

        this.apiKey = apiKey;
    }


    public String getApikey() {

        return this.apiKey;
    }

    public void setSilentFails(boolean silentFails) {

        this.silentFails = silentFails;
    }

    public boolean getSilentFails() {

        return this.silentFails;
    }


    public String getOutputFormat() {

        return this.output_format;
    }

    public void setOutputFormat(String output_format) {

        this.output_format = output_format;
    }


    public void setWait(int wait) {

        this.wait = wait;

    }

    public double getWait() {

        return this.wait;
    }


    public int getMaxRetries() {

        return this.max_retries;

    }

    public void setMaxRetries(int max_retries) {

        this.max_retries = max_retries;
    }


    public float getSleep() {

        return this.sleep;
    }

    public void setSleep(float sleep) {

        this.sleep = sleep;
    }

    public static NeokamiRequest Factory() {

        return new NeokamiRequest();

    }

    public String getOutputType() {

        return this.output_type;
    }

    public void setOutputType(String output_type) throws NeokamiParametersException {

        ArrayList<String> validTypes = new ArrayList<String>();
        validTypes.add("memory");
        validTypes.add("rabbitmq");

        if (!validTypes.contains(output_type)) {

            throw new NeokamiParametersException("Specified output is not valid. Valid types are " + validTypes);
        }
        this.output_type = output_type;

    }

    public boolean checkFilepath(String filePath) throws NeokamiParametersException {

        if (filePath == null || filePath.trim().length() == 0) {

            throw new NeokamiParametersException("Missing Filepath!");
        }
        return true;
    }

    public boolean checkInputStream(byte[] fileStream) throws NeokamiParametersException {

        if (fileStream.length == 0) {

            throw new NeokamiParametersException("No compatible data found, try analyzing a file from disk instead.");
        }
        return true;
    }


    public boolean checkHasAllParameters(Map<String, String> params) throws NeokamiParametersException {

        if (!params.isEmpty()) {


            for (String key : params.keySet()) {

                String value = params.get(key);

                if (value == null || value.trim().length() == 0) {
                    throw new NeokamiParametersException("Missing parameter: " + key);
                }

            }

        }

        return true;
    }

    public NeokamiResponse getResult(String jobId) throws JSONException, NeokamiBaseException {

        HashMap<String, String> hm = new HashMap<String, String>();

        hm.put("job_id", jobId);
        hm.put("api_key", this.apiKey);
        hm.put("sdk_version", Base.SDK_VERSION);
        hm.put("sdk_lang", Base.SDK_LANG);
        String jsonResponse = "";
        try {
            NeokamiCurl neokamiCurl = new NeokamiCurl();
            jsonResponse = neokamiCurl.post(this.getUrl("/engine/job/results"), hm);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new NeokamiResponse(jsonResponse, this.getOutputFormat(), this.getSilentFails());
    }


}
