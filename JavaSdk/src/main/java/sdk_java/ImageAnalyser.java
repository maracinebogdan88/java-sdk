package sdk_java;

/**
 * Copyright 2015 Neokami GmbH.
 */

import org.json.JSONException;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.HttpClients.NeokamiCurl;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class ImageAnalyser extends NeokamiRequest {

    protected byte[] fileStream = new byte[0];
    protected String filePath;


    public NeokamiResponse analyse() throws JSONException, NeokamiBaseException, NeokamiParametersException, IOException {
        if (this.getInputStream().length > 0) {
            return this.analyseFromInputStream();
        }
        return this.analyseFromDisk();
    }


    public NeokamiResponse analyseFromDisk() throws JSONException, NeokamiBaseException, NeokamiParametersException, IOException {
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("wait", String.valueOf(this.getWait()));
        hm.put("max_retries", String.valueOf(this.getMaxRetries()));
        hm.put("sleep", String.valueOf(this.getSleep()));
        hm.put("api_key", this.getApikey());
        hm.put("sdk_version", Base.SDK_VERSION);
        hm.put("sdk_lang", Base.SDK_LANG);

        NeokamiCurl neokamiCurl = new NeokamiCurl();
        String jsonResponse;

        ImageAnalyser imageAnalyser = new ImageAnalyser();
        imageAnalyser.checkFilepath(this.getFilePath());
        imageAnalyser.checkHasAllParameters(hm);
        jsonResponse = neokamiCurl.postBinary(this.getUrl("/analyse/image"), this.getFilePath(), hm);

        return new NeokamiResponse(jsonResponse, this.getOutputFormat(), this.getSilentFails());
    }

    public NeokamiResponse analyseFromInputStream() throws JSONException, NeokamiBaseException, IOException, NeokamiParametersException {
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("wait", String.valueOf(this.getWait()));
        hm.put("max_retries", String.valueOf(this.getMaxRetries()));
        hm.put("sleep", String.valueOf(this.getSleep()));
        hm.put("api_key", this.getApikey());
        hm.put("sdk_version", Base.SDK_VERSION);
        hm.put("sdk_lang", Base.SDK_LANG);

        NeokamiCurl neokamiCurl = new NeokamiCurl();
        String jsonResponse;
        ImageAnalyser imageAnalyser = new ImageAnalyser();

        imageAnalyser.checkInputStream(this.getInputStream());
        imageAnalyser.checkHasAllParameters(hm);
        jsonResponse = neokamiCurl.postBinary(this.getUrl("/analyse/image"), this.getInputStream(), hm);
        return new NeokamiResponse(jsonResponse, this.getOutputFormat(), this.getSilentFails());
    }

    public byte[] getInputStream() {

        return this.fileStream;
    }

    public void setInputStream(byte[] fileStream) throws FileNotFoundException {

        this.fileStream = fileStream;

    }

    public String getFilePath() {

        return this.filePath;
    }

    public void setFilePath(String filePath) {

        this.filePath = filePath;

    }


}
