package sdk_java.Exceptions;

/**
 * Copyright 2015 Neokami GmbH.
 */

public class NeokamiParametersException extends Exception {

    public NeokamiParametersException(String message) {

        super(message);
    }
}
