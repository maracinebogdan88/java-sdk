package sdk_java;

/**
 * Copyright 2015 Neokami GmbH.
 */

import java.io.IOException;

public class Base {

    public static final String API_BASE = "https://api.neokami.io";
    public static final String SDK_VERSION = "0.1";
    public static final String SDK_LANG = "java";

    public String getUrl(String path) throws IOException {
        return API_BASE + path;
    }

}
