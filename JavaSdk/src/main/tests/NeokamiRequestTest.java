import junit.framework.TestCase;
import org.json.JSONException;
import org.junit.Test;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.Exceptions.NeokamiServerException;
import sdk_java.ImageAnalyser;
import sdk_java.NeokamiResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;


/**
 * Copyright 2015 Neokami GmbH.
 */

public class NeokamiRequestTest extends TestCase {

    //Please set the value of the  variable apiKey  to run the sdk tests.
    //if you're developing the sdk, you can get an api key eligible for unlimited calls by emailing us at team@neokami.com

    private static String apiKey="";
    @Test
    public void test_image_from_disk() throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {

        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();
        String[] expectedStatus = {"404", "200"};

        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
        imageAnalyser.setApiKey(apiKey);
        imageAnalyser.setSleep(0.1);

        neokamiResponse=imageAnalyser.analyseFromDisk();
        assertTrue(Arrays.asList(expectedStatus).contains(Integer.toString(neokamiResponse.status())));

        assertNotNull(neokamiResponse.retries());
        assertNotNull(neokamiResponse.createJsonObject(neokamiResponse.warnings()).getInt("retries"));
    }

    @Test
    public void test_image_from_disk_no_wait() throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {


        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();
        String[] expectedStatus = {"404", "200","202"};

        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
        imageAnalyser.setApiKey(apiKey);
        imageAnalyser.setWait(0);

        neokamiResponse=imageAnalyser.analyseFromDisk();
        assertEquals(202, neokamiResponse.status());
        assertEquals(0, neokamiResponse.retries());
        assertNotNull(neokamiResponse.getJobIdResult());
        assertNotNull(neokamiResponse.createJsonObject(neokamiResponse.result()).getString("message"));
        assertNotNull(neokamiResponse.errors());
        System.out.println("jobid" + neokamiResponse.getJobIdResult());
        //if a job has been postponed or whatever we get a job id back
        String jobId=neokamiResponse.getJobIdResult();
        neokamiResponse=imageAnalyser.getResult(jobId);

        //job is either done or not
        assertTrue(Arrays.asList(expectedStatus).contains(Integer.toString(neokamiResponse.status())));
        assertNotNull(neokamiResponse.errors());
        assertEquals("[]", neokamiResponse.warnings());

    }
    @Test
    public void test_postBinary_data_forward() throws NeokamiBaseException, IOException, JSONException, NeokamiParametersException {

        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();
        String[] expectedStatus = {"404", "200"};

        String workingDir = System.getProperty("user.dir");
        Path path = Paths.get(workingDir.concat("/src/main/tests/data/team1.jpg"));
        byte[] data = Files.readAllBytes(path);
        imageAnalyser.setInputStream(data);
        imageAnalyser.setApiKey(apiKey);

        neokamiResponse=imageAnalyser.analyseFromInputStream();
        assertTrue(Arrays.asList(expectedStatus).contains(Integer.toString(neokamiResponse.status())));
    }

    @Test
    public void test_postBinary_data_invalid_data() throws IOException, NeokamiBaseException, JSONException {

            String s="invalid";
            ImageAnalyser imageAnalyser = new ImageAnalyser();
            imageAnalyser.setInputStream(s.getBytes());
            imageAnalyser.setApiKey(apiKey);
        try {
            imageAnalyser.analyseFromInputStream();
        } catch (NeokamiParametersException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test_postBinary_data_no_data() throws IOException, NeokamiBaseException, JSONException {

        byte [] data=new byte[0];
        ImageAnalyser imageAnalyser = new ImageAnalyser();
        imageAnalyser.setInputStream(data);
        imageAnalyser.setApiKey(apiKey);
        try {
            imageAnalyser.analyseFromInputStream();
        } catch (NeokamiParametersException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void test_output_types() throws IOException, NeokamiParametersException, NeokamiBaseException, JSONException {


        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();

        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
        imageAnalyser.setApiKey(apiKey);
        imageAnalyser.setWait(0);
        imageAnalyser.setOutputType("memory");

        neokamiResponse=imageAnalyser.analyseFromDisk();
        assertEquals(202, neokamiResponse.status());


    }

    @Test
    public void test_output_types_false() throws NeokamiParametersException {


        try {
            NeokamiResponse neokamiResponse;
            ImageAnalyser imageAnalyser = new ImageAnalyser();

            String workingDir = System.getProperty("user.dir");
            imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
            imageAnalyser.setApiKey(apiKey);
            imageAnalyser.setWait(0);
            imageAnalyser.setOutputType("invalid");
        }catch (NeokamiParametersException e){
            System.out.println(e.getMessage());
        }
    }


    @Test
    public void test_output_format_json() throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {

        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();

        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
        imageAnalyser.setApiKey(apiKey);
        imageAnalyser.setWait(0);
        imageAnalyser.setOutputFormat("json");
        neokamiResponse=imageAnalyser.analyseFromDisk();
        assertNotNull(neokamiResponse.result());
    }

    @Test
    public void test_no_filePath_post_data()  {

            ImageAnalyser imageAnalyser = new ImageAnalyser();
        try {
            imageAnalyser.analyseFromDisk();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NeokamiBaseException e) {
            e.printStackTrace();
        } catch (NeokamiParametersException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void test_api_key_invalid() {

        NeokamiResponse neokamiResponse;
        ImageAnalyser imageAnalyser=new ImageAnalyser();

        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));

        try {
            neokamiResponse=imageAnalyser.analyseFromDisk();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NeokamiBaseException e) {
            e.printStackTrace();
        } catch (NeokamiParametersException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_missing_params(){
        ImageAnalyser imageAnalyser=new ImageAnalyser();

        String workingDir = System.getProperty("user.dir");
        imageAnalyser.setFilePath(workingDir.concat("/src/main/tests/data/team1.jpg"));
        imageAnalyser.setApiKey("");
        try {
            imageAnalyser.analyseFromDisk();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NeokamiBaseException e) {
            e.printStackTrace();
        } catch (NeokamiParametersException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_empty_response() throws NeokamiBaseException, JSONException {
        try {
            StringBuilder sb = new StringBuilder();
            NeokamiResponse nr = new NeokamiResponse(sb);
        }catch (NeokamiServerException e){
            System.out.println(e);
        }
    }

    @Test
    public void test_no_data() throws NeokamiBaseException,NeokamiParametersException {

        try {
            ImageAnalyser imageAnalyser = new ImageAnalyser();
            NeokamiResponse neokamiResponse = null;
            try {
                neokamiResponse = imageAnalyser.getResult("1");
                assertTrue(neokamiResponse.hasError());
            }catch (NeokamiServerException e){
                System.out.println(e);
            }

        }catch(JSONException e){
            e.getMessage();
        }
    }
}