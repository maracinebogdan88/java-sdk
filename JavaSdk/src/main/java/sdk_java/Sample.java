package sdk_java;

/**
 * Copyright 2015 Neokami GmbH.
 */

import org.json.JSONException;
import org.json.JSONObject;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.HttpClients.NeokamiCurl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class Sample {

    private static String apiKey = "ehu+Y/IQEYLyvlKWCUQOkRBnO4Lq6NKsZ64TNNANz+yGjVye";

    public static void main(String[] args) throws NeokamiBaseException, NeokamiParametersException, JSONException, IOException {
        VisualCortex vc = new VisualCortex();
        String workingDir = System.getProperty("user.dir");
        Path path = Paths.get(workingDir.concat("/src/main/tests/data/apple.jpg"));
        byte[] data = Files.readAllBytes(path);
        vc.setInputStream(data);
        vc.setApiKey(Sample.apiKey);
        vc.setWait(1);
        vc.setMaxRetries(1000);
        vc.setSleep((float) 0.01);
        vc.setModel("custom::231");

        NeokamiResponse analysis = vc.analyse();
        System.out.println(analysis.result());
    }
}
