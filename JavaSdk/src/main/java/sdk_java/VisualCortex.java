package sdk_java;

import org.json.JSONException;
import sdk_java.Exceptions.NeokamiBaseException;
import sdk_java.Exceptions.NeokamiParametersException;
import sdk_java.HttpClients.NeokamiCurl;

import java.io.IOException;
import java.util.HashMap;

public class VisualCortex extends ImageAnalyser {

    public static final String API_BASE = "https://api.visualcortex.io";
    protected String model;

    public String getUrl(String path) throws IOException {
        return API_BASE + path;
    }

    public VisualCortex setModel(String model) {
        this.model = model;
        return this;
    }

    public String getModel() {
        return this.model;
    }

    public NeokamiResponse analyseFromDisk() throws JSONException, NeokamiBaseException, NeokamiParametersException, IOException {
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("wait", String.valueOf(this.getWait()));
        hm.put("max_retries", String.valueOf(this.getMaxRetries()));
        hm.put("sleep", String.valueOf(this.getSleep()));
        hm.put("api_key", this.getApikey());
        hm.put("sdk_version", Base.SDK_VERSION);
        hm.put("sdk_lang", Base.SDK_LANG);
        hm.put("model", this.getModel());

        NeokamiCurl neokamiCurl = new NeokamiCurl();
        String jsonResponse;

        VisualCortex vc = new VisualCortex();
        vc.checkFilepath(this.getFilePath());
        vc.checkHasAllParameters(hm);
        jsonResponse = neokamiCurl.postBinary(this.getUrl("/cortex/analyse"), this.getFilePath(), hm);

        return new NeokamiResponse(jsonResponse, this.getOutputFormat(), this.getSilentFails());
    }

    public NeokamiResponse analyseFromInputStream() throws JSONException, NeokamiBaseException, IOException, NeokamiParametersException {
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("wait", String.valueOf(this.getWait()));
        hm.put("max_retries", String.valueOf(this.getMaxRetries()));
        hm.put("sleep", String.valueOf(this.getSleep()));
        hm.put("api_key", this.getApikey());
        hm.put("sdk_version", Base.SDK_VERSION);
        hm.put("sdk_lang", Base.SDK_LANG);
        hm.put("model", this.getModel());

        NeokamiCurl neokamiCurl = new NeokamiCurl();
        String jsonResponse;
        ImageAnalyser imageAnalyser = new ImageAnalyser();

        imageAnalyser.checkInputStream(this.getInputStream());
        imageAnalyser.checkHasAllParameters(hm);
        jsonResponse = neokamiCurl.postBinary(this.getUrl("/cortex/analyse"), this.getInputStream(), hm);
        return new NeokamiResponse(jsonResponse, this.getOutputFormat(), this.getSilentFails());
    }


}
