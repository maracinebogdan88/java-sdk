package sdk_java.Exceptions;

/**
 * Copyright 2015 Neokami GmbH.
 */

import org.json.JSONException;
import org.json.JSONObject;


public class NeokamiBlockedException extends NeokamiBaseException {

    public NeokamiBlockedException(JSONObject jsonResponseObj, int code) throws JSONException {

        super(jsonResponseObj, code);
    }
}
