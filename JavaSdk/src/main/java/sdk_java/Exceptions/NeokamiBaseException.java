package sdk_java.Exceptions;

/**
 * Copyright 2015 Neokami GmbH.
 */

import org.json.JSONException;
import org.json.JSONObject;

public class NeokamiBaseException extends Exception {

    private int code;
    private JSONObject jsonResponseObj = null;

    public NeokamiBaseException(JSONObject jsonResponseObj, int code) throws JSONException {

        super(checkJsonObject(jsonResponseObj));
        this.code = code;
        this.jsonResponseObj = jsonResponseObj;

    }

    private static String checkJsonObject(JSONObject jsonResponseObj) throws JSONException {

        StringBuilder message = new StringBuilder();
        message.append("Malformed server response, please try again later.");

        if (jsonResponseObj != null) {

            message.setLength(0);
            message.append("");
            String errors = "";
            if (jsonResponseObj.has("errors")) {
                errors = jsonResponseObj.getString("errors");

            }
            message.append(errors);
        }

        return message.toString();

    }

    public boolean isMalformed() {

        return this.jsonResponseObj == null || this.jsonResponseObj.length() == 0;
    }

    public String getError() throws JSONException {

        if (!this.isMalformed()) {
            return this.jsonResponseObj.getString("errors");
        }
        return null;
    }

    public int getStatusCode() throws JSONException {

        return this.jsonResponseObj.getInt("status_code");

    }

    public String getWarnings() throws JSONException {

        if (!this.isMalformed()) {
            return this.jsonResponseObj.getString("warnings");
        }
        return null;
    }


}
